#!/bin/bash

#set -eu
#source /usr/src/project/.env
# echo "Create MapStore2 project, if it does not exist..."
# if [ -f /usr/src/project/package.json ]; then
# 	echo "Project exists"
# else
#   # Create the project
#   cd /usr/src
#   rm MapStore2
#   git clone $mapstore2_repo MapStore2
#   cd /usr/src/MapStore2
#   git checkout $mapstore2_tag
#   npm install
#   node createProject.js "$project_name" "$project_version" "$project_description" "$project_repo" "$project_dir"
#   # Build/Install the frontend code
#   cd /usr/src/project/
#   sed -i "s/--port/--host 0.0.0.0 --port/" package.json
#   cd /usr/src/project/MapStore2
#   git checkout $mapstore2_tag
#   cd /usr/src/project/
#   npm install
#   npm run compile
#   npm run lint
#   # Build/Install the Java code
#   mvn clean install
#   ln /usr/src/project/web/target/$project_name.war /usr/local/tomcat/webapps/$project_name.war
#   rm -r /usr/src/project/web/target
#   # Clean up
#   rm -r /usr/src/MapStore2
#   ln -s /usr/src/project/MapStore2 /usr/src/MapStore2
# fi

echo "Starting Tomcat..."
/usr/local/tomcat/bin/startup.sh

echo "Starting MapStore2 project..."
cd /usr/src/project
npm start

