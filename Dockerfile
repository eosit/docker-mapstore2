FROM tomcat:7-alpine

ARG http_proxy=""
ARG no_proxy=""

ARG mapstore2_repo="https://github.com/geosolutions-it/MapStore2.git"
ARG mapstore2_tag="master"

ARG project_name="ci_test_project"
ARG project_version="0.0.1"
ARG project_description="CI Test Project"
ARG project_repo="https://test.ci/repo.git"
ARG project_dir="/usr/src/project"

# Install alpine packages
RUN apk -qq update
RUN apk add --update \
    bash \
    curl \
    git \
    nodejs \
    nodejs-npm \
    maven \
    rsync \
  && rm -rf /var/cache/apk/*

# Use fs-extra for check cesium script
RUN cd $(npm root -g)/npm \
  && npm install fs-extra \
  && sed -i -e s/graceful-fs/fs-extra/ -e s/fs\.rename/fs.move/ ./lib/utils/rename.js

# Use NPM version 3
RUN npm config -g set loglevel info
RUN npm set -g registry https://registry.npmjs.org/
RUN npm install -g npm@3.x

# Add project dir
RUN mkdir -p /usr/src/project
ADD . /usr/src/project

# Create MapStore2 project, if it does not exist.
RUN test -f /usr/src/project/package.json \
  || mkdir -p /usr/src/MapStore2 \
  && cd /usr/src/MapStore2 \
  && git clone $mapstore2_repo . \
  && git checkout $mapstore2_tag \
  && npm install \
  && node createProject.js "$project_name" "$project_version" "$project_description" "$project_repo" "/usr/src/project" \
  && cd /usr/src/project/ \
  && sed -i "s/--port/--host 0.0.0.0 --port/" package.json \
  && cd /usr/src/project/MapStore2 \
  && git checkout $mapstore2_tag \
  && rm -r /usr/src/MapStore2 \
  && ln -s /usr/src/project/MapStore2 /usr/src/MapStore2

# Install project node_modules
WORKDIR /usr/src/project
RUN npm install
RUN npm run compile
RUN npm run lint

# Build WAR file
WORKDIR /usr/src/project
RUN mvn clean install \
  && ln /usr/src/project/web/target/$project_name.war /usr/local/tomcat/webapps/$project_name.war \
  && rm -r /usr/src/project/web/target

# Startup script
COPY ./startup.sh /
CMD [ "bash", "startup.sh" ]

