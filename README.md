Dockerized MapStore2 project 
============================

This is a dockerized MapStore2 project.


Instructions for starting a new project:
----------------------------------------

To use this repo as a starting point for your own MapStore2 project follow these instructions:

1. Create a fork of this repo and clone to your computer
2. Change the values in `.env` to define your project
3. Prepare the Docker image by doing `docker-compose build` or `docker-compose pull` (make sure the docker registry specified in `.env` contains the image)
3. Copy the files created by the "createProject" script into your repo by running `docker-compose run sync yes`.
4. Add the newly created project to Git using `git add .` and `git commit`



Instructions for existing projects:
-----------------------------------


### Clone:


Make sure to add `--recursive`!

    git clone --recursive git@gitlab.com:eosit/docker-mapstore2.git

or

    git clone --recursive https://gitlab.com/eosit/docker-mapstore2.git



### Build:


Locally:

    docker-compose build


Using gitlab.com CI, after pushing new commits to gitlab.com:

    docker-compose pull


After building a newly created project for the first time a synchronization between 
the baked-in files and the project repo needs to be done. This is because we cant
mount a volume during build time. This can also be used when changes to the Dockerfile
needs to be affected in the project repo.

    docker-compose run --rm sync rsync -rvv --times /usr/src/project/ /tmp/project/ | grep -v uptodate | grep -v skipping

or simply

	docker-compose run sync project

This will do an rsync to copy/replace files in the project repo with the files baked into
the Docker image. Use with caution! Make sure project files are committed to git before
running this command so that you can merge changes manually if needed.


### Run:


    docker-compose up
    
Browse to http://localhost:8081



