#!/bin/bash

# See README.md for usage info

if [ "$1" == "yes" ]; then
  rsync -rvv --times --ignore-existing /usr/src/project/ /tmp/project/ | grep -v uptodate | grep -v skipping
fi

